package com.example.lesson5_spring.repository;

import com.example.lesson5_spring.courses.Course;
import com.example.lesson5_spring.exceptions.CourseNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Component
public class InMemoryCourseRepository implements CourseRepository {
    private final List<Course> courses;
    private final AtomicLong idGenerator = new AtomicLong(0);

    public InMemoryCourseRepository() {
        courses = new CopyOnWriteArrayList<>();
        courses.add(new Course(idGenerator.incrementAndGet(),
                "Петров А.В.", "Основы кройки и шитья", "Шьет и кроет"));
        courses.add(new Course(idGenerator.incrementAndGet(),
                "Мошкина А.В", "Введение в архитектурный дизайн", "Что-то на умном"));
    }

    @Override
    public Optional<Course> findById(Long id) {
        return courses.stream()
                .filter(c -> Objects.equals(c.getId(), id))
                .findFirst();
    }

    @Override
    public List<Course> findAll() {
        // чтобы случайное изменение в полученном списке не поменяло на данные внутри InMemoryCourseRepository
        return new ArrayList<>(courses);
    }

    @Override
    public Course save(Course course) {
        // если id == null, курс новый
        if (course.getId() == null) {
            course.setId(idGenerator.incrementAndGet());
            courses.add(course);
            // чтобы изменение в Course не повлияло на состояние в InMemoryCourseRepository
            return new Course(course);
        }
        // иначе - это операция update
        else {
            synchronized (this) {
                for (Course c : courses) {
                    if (Objects.equals(c.getId(), course.getId())) {
                        c.setAuthor(course.getAuthor());
                        c.setTitle(course.getTitle());
                        return course;
                    }
                }
                // иначе курс не найден
                throw new CourseNotFoundException("No course with id=" + course.getId());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        courses.remove(findById(id).orElseThrow());
    }

    @Override
    public List<Course> findByTitleWithPrefix(String prefix) {
        return courses.stream()
                .filter(course -> course.getTitle().startsWith(prefix))
                .collect(Collectors.toList());
    }
}
