package com.example.lesson5_spring.services;

import com.example.lesson5_spring.courses.Course;
import com.example.lesson5_spring.courses.CourseRequestToChange;
import com.example.lesson5_spring.repository.CourseRepository;
import com.example.lesson5_spring.repository.InMemoryCourseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    public CourseService(InMemoryCourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public void updateCourse(Long id, CourseRequestToChange request) {
        Course course = courseRepository.findById(id).orElseThrow();
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());
        course.setDescription(request.getDescription());
    }
    public void createCourse(CourseRequestToChange request) {
        Course course = new Course();
        course.setDescription(request.getDescription());
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());
        courseRepository.save(course);
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findById(Long id) {
        return courseRepository.findById(id).orElseThrow();
    }

    private void save(Course course) {
        courseRepository.save(course);
    }

    public void deleteById(Long id) {
        courseRepository.deleteById(id);
    }

    public List<Course> findByPrefix(String prefix) {
        return courseRepository.findByTitleWithPrefix(prefix);
    }

}
