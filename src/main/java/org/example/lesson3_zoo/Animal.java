package org.example.lesson3_zoo;

public abstract class Animal {
    protected String species;
    protected String name;
    protected Boolean vegetarian;
    protected String modeOfMoving;

    public abstract void makeNoise();
    public void feed(Food food) {
        switch (food) {
            case MEAT:
                if (vegetarian) {
                    System.out.println("I am veggi, only broccoli, pls");
                } else {
                    System.out.println("Сюда-а-а-а");
                }
                break;

            case GRASS:
                if (vegetarian) {
                    System.out.println("Сюда-а-а-а");
                } else {
                    System.out.println("It is my last caution, bring me meat or I will eat those kids.");
                }
                break;

            default:
                System.out.println("Animals here don't know this food!");
        }
    }

    public String getName() {
        return name;
    }
    public String getModeOfMoving() {
        return modeOfMoving;
    }
    public String getSpecies() {
        return species;
    }

    public void isVegeterian() {
        System.out.print("I am " + this.name + " and I am ");
        if (vegetarian) {
            System.out.println("a vegetarian and don't eat even a trace of meat!");
        } else {
            System.out.println("a meatatarian and eat meat from time to time!");
        }
    }


    public void howMove() {
        System.out.println("People call me: a " + this.species +
                " and I travel by " + modeOfMoving + "!");
    }

    public void info() {
        System.out.print("What animal am I? I am a " + this.species + ".\n");
        System.out.print("A few more facts about me:\n");
        howMove();
        isVegeterian();
        System.out.print("Also, I make weird noise, such as: ");
        makeNoise();
        System.out.println();
        System.out.print("Okay guys, moving next:");
        System.out.println();
    }

}

