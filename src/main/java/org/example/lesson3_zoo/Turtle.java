package org.example.lesson3_zoo;

public class Turtle extends Animal {
    @Override
    public void makeNoise() {
        System.out.println("You so totally rock, Squirt! So gimme some fin");
    }

    public Turtle(String name) {
        species = "turtle";
        vegetarian = true;
        modeOfMoving = "swimming, but not like stupid dolphins do";
        this.name = name;
    }
}
