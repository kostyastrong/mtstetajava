package org.example.lesson4_java_core;

import java.util.concurrent.BlockingQueue;

public abstract class UserGenerator implements Runnable {
    private final BlockingQueue<Message> queue;
    int id = 0;

    public UserGenerator(BlockingQueue<Message> queue, int id) {
        this.queue = queue;
        this.id = id;
    }

    abstract Message generateUser();

    @Override
    public void run() {
        try {
            System.out.println("inside gen");
            queue.add(generateUser());
        } catch (Exception e) {
            System.out.println("user gen error");
        }
    }
}
