package org.example.lesson3_zoo;

public class Tiger extends Animal{
    public Tiger(String name) {
        species = "tiger";
        modeOfMoving = "legs, overland";
        vegetarian = false;
        this.name = name;
    }
    @Override
    public void makeNoise() {
        System.out.println("gr-r-r-r");
    }
}
