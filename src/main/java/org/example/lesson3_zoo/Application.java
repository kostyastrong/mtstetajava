package org.example.lesson3_zoo;

public class Application {
    public static void main(String[] args) {
        System.out.println("Welcome to the zoo!\n");
        Camel camel = new Camel("Bob");
        Eagle eagle = new Eagle("Scream");
        Horse horse = new Horse("Ronaldo");
        Tiger tiger = new Tiger("Tigger");   // Winnie-the-Pooh!
        Turtle turtle = new Turtle("Crush");   // Nemo!  Sorry, dolphins are banned in zoos in my world

        camel.info();
        eagle.info();
        horse.info();
        tiger.info();
        turtle.info();
        System.out.println("Whoops, seems like it was the end, no more animals. Sorry, kids.\n");

        // now, after kids gone, let's feed the animals with grass or meat!
        tiger.feed(Food.GRASS);
        tiger.feed(Food.MEAT);

        System.out.println();

        turtle.feed(Food.GRASS);
        turtle.feed(Food.MEAT);
    }
}