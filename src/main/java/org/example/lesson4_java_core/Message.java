package org.example.lesson4_java_core;

public class Message {
    private String content_;
    public final EnrichmentType enrichmentType;
    public static int globalId;
    private final int id;

    public Message(String content, EnrichmentType enrichmentType) {  // no multithreads because object is new
        content_ = content;
        this.id = globalId;
        globalId += 1;
        this.enrichmentType = enrichmentType;
    }
    public enum EnrichmentType {
        MSISDN,
        SMTH
    }

    public String getContent() {
        return content_;
    }

    public void setContent(String content) {
        synchronized (this) {
            content_ = content;
        }
    }
}
