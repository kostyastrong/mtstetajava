package com.example.lesson5_spring.repository;

import com.example.lesson5_spring.courses.Course;
import java.util.List;
import java.util.Optional;

public interface CourseRepository {
    Optional<Course> findById(Long id);

    List<Course> findAll();
    Course save(Course course);

    void deleteById(Long id);

    List<Course> findByTitleWithPrefix(String prefix);
}




