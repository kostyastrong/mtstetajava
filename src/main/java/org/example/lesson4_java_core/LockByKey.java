package org.example.lesson4_java_core;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockByKey {
    private ConcurrentHashMap<String, LockWrapper> locks = new ConcurrentHashMap<String, LockWrapper>();
    private static class LockWrapper {  // why static is recommended?
        private final Lock lock = new ReentrantLock();
        private final AtomicInteger numberInQueue = new AtomicInteger(1);  // why final is recommended?
        private LockWrapper addThreadInQueue() {
            numberInQueue.incrementAndGet();
            return this;
        }
        private int removeThreadFromQueue() {
            return numberInQueue.decrementAndGet();
        }
    }
    public void lock(String key) {
        LockWrapper lockWrapper = locks.compute(key, (k, v) -> v == null ? new LockWrapper() : v.addThreadInQueue());
        lockWrapper.lock.lock();  // thread will stop here until it is available?
    }
    public void unlock(String key) {
        LockWrapper lockWrapper = locks.get(key);
        lockWrapper.lock.unlock();
        if (lockWrapper.removeThreadFromQueue() == 0) {
            locks.remove(key);  // otherwise we should deal with 0 and null in lock(), which is the same here
        }
    }
}
