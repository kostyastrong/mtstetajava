package org.example.lesson3_zoo;

public class Horse extends Animal {
    public Horse(String name) {
        species = "horse";
        modeOfMoving = "long legs, overland";
        vegetarian = true;
        this.name = name;
    }
    @Override
    public void makeNoise() {
        System.out.println("ia-a-a-ah");
    }
}
