package com.example.lesson5_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })  // exclude = {DataSourceAutoConfiguration.class }
public class Lesson5SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson5SpringApplication.class, args);
    }

}
