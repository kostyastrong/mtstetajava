package com.example.lesson5_spring.exceptions;

public class CourseNotFoundException extends RuntimeException {
    private final String info;
    public CourseNotFoundException(String s) {
        this.info = s;
    }

    public String GetError() {
        return this.info;
    }
}