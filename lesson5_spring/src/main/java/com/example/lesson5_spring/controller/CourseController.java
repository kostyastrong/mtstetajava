package com.example.lesson5_spring.controller;

import com.example.lesson5_spring.courses.Course;
import com.example.lesson5_spring.courses.CourseRequestToChange;
import com.example.lesson5_spring.exceptions.ApiError;
import com.example.lesson5_spring.repository.CourseRepository;
import com.example.lesson5_spring.repository.InMemoryCourseRepository;
import com.example.lesson5_spring.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/course")
public class CourseController {
    private final CourseService courseService;

    @Autowired
    public CourseController(CourseRepository courseRepository) {
        this.courseService = new CourseService((InMemoryCourseRepository) courseRepository);
    }

    @GetMapping("")
    public List<Course> courseTable() {
        return courseService.findAll();
    }

    @GetMapping("/{id}")
    public Course getCourse(@PathVariable("id") Long id) {
        return courseService.findById(id);
    }

    @GetMapping("/prefix")
    public List<Course> getByPrefix(@RequestBody String prefix) {
        return courseService.findByPrefix(prefix);
    }

    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @Valid @RequestBody CourseRequestToChange request) {
        courseService.updateCourse(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.deleteById(id);
    }

    @PostMapping()
    public void createCourse(@Valid @RequestBody CourseRequestToChange request) {
        courseService.createCourse(request);
    }

    @ExceptionHandler
    public ResponseEntity<ApiError> noSuchElementExceptionHandler(NoSuchElementException ex) {
        return new ResponseEntity<>(
                new ApiError(HttpStatus.NOT_FOUND, "page not found", "404 error"),
                HttpStatus.NOT_FOUND
        );  // RepsonseEntity.notFound().build(), what's the difference??? TODO
    }

    @ExceptionHandler
    public ResponseEntity<ApiError> noSuchElementExceptionHandler(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ApiError(),
                HttpStatus.BAD_REQUEST
        );
    }
}

