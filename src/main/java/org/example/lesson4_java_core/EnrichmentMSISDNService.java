package org.example.lesson4_java_core;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public class EnrichmentMSISDNService extends EnrichmentService {
    static ConcurrentHashMap<String, JSONObject> telephoneBook = new ConcurrentHashMap<>();
    private LockByKey lockByKey = new LockByKey();  // https://www.baeldung.com/java-acquire-lock-by-key - theory

    public EnrichmentMSISDNService(BlockingQueue<Message> queue,
                                   List<Message> success, List<Message> failed) {
        super(queue, success, failed);
    }

    // first, I wrote this genius map with lock on writing only one element, then I found that hashmap has .replace
    // method, is it the same with what I wanted or different from the article (the link on the 15th line here)
    private void changeData(String telNumber, JSONObject data) {
        lockByKey.lock(telNumber);
        telephoneBook.replace(telNumber, telephoneBook.get(telNumber), data);
        lockByKey.unlock(telNumber);
    }

    public void addData(String telNumber, JSONObject data) {
        if (telephoneBook.containsKey(telNumber)) {
            changeData(telNumber, data);
        } else {
            telephoneBook.put(telNumber, data);
        }
    }

    public JSONObject getJSONObject(String json) {
        try {
            return new JSONObject(json);
        } catch (JSONException e) {
            return new JSONObject();
        }
    }

    public boolean isValidMSISDN(Message message) {
        JSONObject jsonMessage = getJSONObject(message.getContent());
        try {
            jsonMessage.getString("msisdn");
            return true;
        } catch (Exception error) {
            return false;  // not valid, that's all:)
        }
    }

    public String getMSISDN(Message message) {
        JSONObject jsonMessage = getJSONObject(message.getContent());
        try {
            return jsonMessage.getString("msisdn");
        } catch (JSONException e) {
            return null;
        }
    }

    public Boolean enrich(Message message) {
        if (isValidMSISDN(message)) {  // is Dependency Injection rule broken by this?
            JSONObject jsonMessage = getJSONObject(message.getContent());
            try {
                if (message.enrichmentType == Message.EnrichmentType.MSISDN) {
                    jsonMessage.put("enrichment", telephoneBook.get(getMSISDN(message)));
                    message.setContent(jsonMessage.toString());
                }
                return true;
            } catch (JSONException e) {
                System.out.println("enrichment error");
                return false;
            }
        }
        return false;
    }


}
