package org.example.lesson3_zoo;

public class Eagle extends Animal{

    public Eagle(String name) {
        species = "eagle";
        modeOfMoving = "wings, like a boss";
        vegetarian = false;
        this.name = name;
    }
    @Override
    public void makeNoise() {
        System.out.println("carrrrrr");
    }

    @Override
    public void howMove() {
        System.out.println("People call me: an " + this.species +
                " and I travel by " + modeOfMoving + "!\n");
    }

    @Override
    public void info() {
        System.out.print("What animal am I? I am an " + this.species + ".\n");
        System.out.print("A few more facts about me:\n");
        howMove();
        isVegeterian();
        System.out.print("Also, I make weird noise, such as: ");
        makeNoise();
        System.out.println();
        System.out.print("Okay guys, moving next:");
        System.out.println();
    }

}
