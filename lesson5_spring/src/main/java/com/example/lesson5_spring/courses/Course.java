package com.example.lesson5_spring.courses;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Course {
    Long id;
    String author;
    String title;
    String description;

    public Course(Course course) {
        this.id = course.getId();
        this.author = course.getAuthor();
        this.title = course.getTitle();
        this.description = course.description;
    }
}