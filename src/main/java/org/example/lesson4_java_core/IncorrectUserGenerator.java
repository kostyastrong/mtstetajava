package org.example.lesson4_java_core;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class IncorrectUserGenerator extends UserGenerator{
    private static AtomicInteger globalID = new AtomicInteger(0);

    public IncorrectUserGenerator(BlockingQueue<Message> queue, int id) {
        super(queue, id);
    }
    @Override
    Message generateUser() {
        globalID.incrementAndGet();
        return new Message("{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card" + globalID + "\",\n" +
                "}", Message.EnrichmentType.MSISDN);
    }
    Message noMSISDNType() {
        globalID.incrementAndGet();
        return new Message("{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card" + globalID + "\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}", Message.EnrichmentType.SMTH);
    }
    Message wrongJSONData() {
        globalID.incrementAndGet();
        return new Message("{\n" +
                "    \"acti:on\": \"button_click\",\n" +
                "    \"page\": \"book_card" + globalID + "\",\n" +
                "}", Message.EnrichmentType.MSISDN);
    }
}
