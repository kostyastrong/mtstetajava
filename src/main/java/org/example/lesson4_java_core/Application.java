package org.example.lesson4_java_core;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

public class Application {
    public static List<Message> start() throws InterruptedException {
        List<Message> success = Collections.synchronizedList(new ArrayList<>());
        List<Message> failed = Collections.synchronizedList(new ArrayList<>());
        BlockingQueue<Message> queue = new LinkedBlockingQueue<>();

        EnrichmentMSISDNService MSISDNServ = new EnrichmentMSISDNService(queue, success, failed);
        JSONObject creditInfo = null;
        try {
            creditInfo = new JSONObject("{\n" +
                    "        \"firstName\": \"Vasya\",\n" +
                    "        \"lastName\": \"Ivanov\"\n" +
                    "    }");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        MSISDNServ.addData("88005553535", creditInfo);
        CreditUserGenerator creditUserGenerator = new CreditUserGenerator(queue, 1);


        int n_processors = Runtime.getRuntime().availableProcessors();
        ExecutorService es = Executors.newFixedThreadPool(n_processors);
        // ExecutorCompletionService ecs = new ExecutorCompletionService(es);  // I read that somehow i can apply it
        System.out.println("about to submit");

        for (int i = 0; i < 10; i++) {
            CompletableFuture.runAsync(creditUserGenerator, es);
        }
        TimeUnit.MILLISECONDS.sleep(500);

        while (! queue.isEmpty()) {
            CompletableFuture.runAsync(MSISDNServ, es);
        }
        TimeUnit.MILLISECONDS.sleep(500);
        es.shutdown();

        System.out.println(success.size());
        System.out.println("submitted");
        return success;
        //return failed;
    }
    public static void main(String[] args) throws InterruptedException {
        start();
    }
}
