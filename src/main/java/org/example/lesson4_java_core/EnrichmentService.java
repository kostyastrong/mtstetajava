package org.example.lesson4_java_core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class EnrichmentService implements Runnable{
    private final BlockingQueue<Message> queue;
    List<Message> successfulEnrichedMessages;
    List<Message> failedEnrichedMessages;


    public EnrichmentService(BlockingQueue<Message> queue, List<Message> s, List<Message> f) {
        this.queue = queue;
        this.successfulEnrichedMessages = s;
        this.failedEnrichedMessages = f;
    }  // I wanted to make it with queue inside, but then I've decided to implement the solution with future

    // возвращается обогащенный (или необогащенный content сообщения)
    abstract Boolean enrich(Message message);

    @Override
    public void run() {
        try {
            Message cur = queue.poll(1000, TimeUnit.MILLISECONDS);
            Boolean result = enrich(cur);
            if (result) {
                successfulEnrichedMessages.add(cur);
            } else {
                failedEnrichedMessages.add(cur);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}


