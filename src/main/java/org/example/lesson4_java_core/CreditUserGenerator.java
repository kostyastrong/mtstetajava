package org.example.lesson4_java_core;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class CreditUserGenerator extends UserGenerator{
    private static AtomicInteger globalID = new AtomicInteger(0);

    public CreditUserGenerator(BlockingQueue<Message> queue, int id) {
        super(queue, id);
    }
    @Override
    Message generateUser() {
        globalID.incrementAndGet();
        return new Message("{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card" + globalID + "\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}", Message.EnrichmentType.MSISDN);
    }
}
