package org.example.lesson3_zoo;

public class Camel extends Animal{
    public Camel(String name) {
        species = "camel";
        modeOfMoving = "legs, overland";
        vegetarian = true;
        this.name = name;
    }

    @Override
    public void makeNoise() {
        System.out.println("phshsh");
    }
}
