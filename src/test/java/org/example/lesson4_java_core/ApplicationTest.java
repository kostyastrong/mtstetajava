package org.example.lesson4_java_core;

import org.example.lesson4_java_core.Application;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    @Test
    void start() throws InterruptedException {
        List<Message> success = Application.start();
        assertEquals(10, success.size());
        Message firstMessage = (Message) success.toArray()[0];
        System.out.println(firstMessage.getContent());
    }
}