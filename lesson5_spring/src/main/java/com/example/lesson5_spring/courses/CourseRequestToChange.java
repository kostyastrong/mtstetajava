package com.example.lesson5_spring.courses;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Getter @Setter
@Component
public class CourseRequestToChange {

    @NotBlank(message="no author")
    private String author;

    @NotBlank(message="no title")
    private String title;

    @NotBlank(message="no discription")
    private String description;
//    private Integer id;
}
